<?php
namespace App\Controller;

use App\Services\View\View;
use App\Utility\Asset;

class HomeController{
    public function index($request)
    {
//        $data <- Repository ;
//        $view <- $data ;
        $data = [
            'name' => 'Loghman Avand',
            'numCourse' => 5,
        ];

        $text = View::render('sms.after-registration',$data) ;
        Asset::js("app");
        Asset::css("main");
        Asset::image("icons.add.png");
//        sendSMS($text,"0987654321");
        View::load("home.index",$data,"front");
//        $str = View::render("home.index",$data);
//        echo $str;
    }
}