<?php
namespace App\Services\View;

class View{

    public static function is_valid($viewName)
    {
        $viewName     = str_replace(".","/",$viewName);
        $fullViewPath = VIEW_PATH . $viewName . ".php";
        if(!file_exists($fullViewPath) or !is_readable($fullViewPath)){
            return false;
        }
        return $fullViewPath;
    }
    public static function load($viewName,array $data = array(),$layout = null)
    {
        $fullViewPath = self::is_valid($viewName);
        if(!$fullViewPath){
            echo "View Not Valid !";
            die();
        }
        extract($data);
        if(is_null($layout)){
            include $fullViewPath;
        }else{
            $fullLayoutPath = VIEW_PATH . "layouts/" . $layout . ".php";
            $renderedView = self::render($viewName,$data);
            include $fullLayoutPath;
        }
    }

    public static function render($viewName,array $data = array())
    {
        $fullViewPath = self::is_valid($viewName);
        if(!$fullViewPath){
            echo "View Not Valid !";
            die();
        }
        extract($data);
        ob_start();
        include $fullViewPath;
        return ob_get_clean();
    }


}
