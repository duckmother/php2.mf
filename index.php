<?php
error_reporting(E_ALL);
ini_set('display_errors',1);

require_once "bootstrap/Constants.php";
require_once "config/parameters.php";
require_once "autoload.php";

\App\Services\Router\Router::start();