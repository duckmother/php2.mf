<?php
return [
    '/php2.mf/' => [
        'method' => 'get',
        'target' => 'HomeController@index',
        'middleware' => 'IEBlocker',
    ],
    '/free' => [
        'method' => 'get',
        'target' => 'HomeController@index',
    ],
    '/archive' => [
        'method' => 'get|post',
        'target' => 'PostController@archive'
    ],
    '/user/login' => [
        'method' => 'get',
        'target' => 'UserController@loginform'
    ],
    '/user/dologin' => [
        'method' => 'post',
        'target' => 'UserController@login',
        'middleware' => 'Sanitize',
    ],
];

